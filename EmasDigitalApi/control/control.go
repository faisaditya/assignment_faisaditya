package controlls

import (
	"emasdigitalapi/models"
	"encoding/json"
	"fmt"
	"net/http"
)

type responseCheckHarga struct {
	Error bool               `json:"error"`
	Data  models.MCheckharga `json:"data"`
}
type responseSaldo struct {
	Error bool          `json:"error"`
	Data  models.MSaldo `json:"data"`
}
type responseInputHargaS struct {
	Error  bool   `json:"error"`
	Reffid string `json:"reff_id"`
}
type responseInputHargaF struct {
	Error   bool   `json:"error"`
	Reffid  string `json:"reff_id"`
	Message string `json:"message"`
}

func Checkharga(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	res := responseCheckHarga{}
	res.Error = false
	res.Data = models.Checkharga()
	json.NewEncoder(w).Encode(res)
}

func Inputharga(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	var inharga models.MInputharga
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&inharga)
	if err != nil {
		fmt.Println("test error :", err)
	}

	errors, idref := "", ""
	//cek error
	idref, errors = models.Inputharga(inharga)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Bukan admin"
		json.NewEncoder(w).Encode(resf)
	}

}

func Topup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var mtopup models.MTopup
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&mtopup)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	errors, idref := "", ""
	idref, errors = models.Topup(mtopup)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Harga dimasukan berbeda dengan harga_topup saat ini"
		json.NewEncoder(w).Encode(resf)
	}
}

func Buyback(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var mtopup models.MTopup
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&mtopup)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	errors, idref := "", ""
	idref, errors = models.Buyback(mtopup)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Saldo salah"
		json.NewEncoder(w).Encode(resf)
	}
}

func Saldo(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	var m models.MSaldo
	err := json.NewDecoder(r.Body).Decode(&m)
	if err != nil {
		fmt.Println("post body error :", err)
	}

	res := responseSaldo{}
	res.Error = false
	res.Data = models.Saldo(m.Norek)
	json.NewEncoder(w).Encode(res)
}
