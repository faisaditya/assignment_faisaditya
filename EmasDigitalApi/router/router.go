package router

import (
	controlls "emasdigitalapi/control"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	srouter := router.PathPrefix("/api").Subrouter() //global url api

	//sub url api
	srouter.HandleFunc("/check-harga", controlls.Checkharga).Methods("GET")
	srouter.HandleFunc("/input-harga", controlls.Inputharga).Methods("POST")

	srouter.HandleFunc("/topup", controlls.Topup).Methods("POST")
	srouter.HandleFunc("/buyback", controlls.Buyback).Methods("POST")

	srouter.HandleFunc("/saldo", controlls.Saldo).Methods("POST")

	return router
}
