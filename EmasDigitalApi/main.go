package main

import (
	"emasdigitalapi/router"
	"fmt"
	"log"
	"net/http"
)

func main() {
	r := router.Router()
	fmt.Print("Server Running On Port : 80")
	log.Fatal(http.ListenAndServe(":80", r))
}
