module emasdigitalapi

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.7
	github.com/teris-io/shortid v0.0.0-20220617161101-71ec9f2aa569
)
