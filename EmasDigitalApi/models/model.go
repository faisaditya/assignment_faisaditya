package models

import (
	"emasdigitalapi/config"
	"fmt"
	"strconv"

	"github.com/teris-io/shortid"
)

type MCheckharga struct {
	HargaBuyBack int `json:"harga_buyback"`
	HargaTopUp   int `json:"harga_topup"`
}

type MInputharga struct {
	AdminId      string `json:"admin_id"`
	HargaBuyBack string `json:"harga_buyback"`
	HargaTopUp   string `json:"harga_topup"`
}

type MTopup struct {
	Gram  string `json:"gram"`
	Harga string `json:"harga"`
	Norek string `json:"norek"`
}

type MSaldo struct {
	Norek string `json:"norek"`
	Saldo string `json:"saldo"`
}

func Checkharga() MCheckharga {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select hargatopup,hargabuyback from tbl_harga"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var mch MCheckharga
	for rows.Next() {
		err = rows.Scan(&mch.HargaTopUp, &mch.HargaBuyBack) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}

	return mch
}

func Inputharga(inharga MInputharga) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""
	//cek admin
	iAdmin := inharga.AdminId
	if iAdmin == "a001" {
		//update tbl harga
		db := config.ConnectDB()
		defer db.Close()
		sql := "update tbl_harga set hargatopup=$1, hargabuyback=$2a"
		_, err := db.Exec(sql, inharga.HargaTopUp, inharga.HargaBuyBack)
		if err != nil {
			fmt.Println("error insert harga : ", err)
		}
	} else {
		sError = "1"
	}
	return refid, sError
}

func Topup(mtopup MTopup) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""

	//ambil harga topup saat ini
	htopup := getHargaTopUp()
	//cek harga topup
	iTopup, _ := strconv.Atoi(mtopup.Harga) //string to int
	if iTopup == htopup {
		//insert tbl_topup
		insertTranTopup(mtopup)
		//hitung saldo norek
		tSaldo := getSumTopup(mtopup.Norek)
		//update tbl_rek norek
		updateRekening(mtopup.Norek, tSaldo)
	} else {
		sError = "1"
	}
	return refid, sError
}

func Buyback(mtopup MTopup) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""

	//ambil harga topup saat ini
	htopup := getHargaTopUp()
	//cek harga topup
	iTopup, _ := strconv.Atoi(mtopup.Harga) //string to int
	if iTopup == htopup {
		//insert tbl_topup
		insertTran(mtopup)
		//hitung saldo norek
		tTop := getSumTopup(mtopup.Norek)
		tBuy := getSumBuy(mtopup.Norek)
		//update tbl_rek norek
		var tSaldo float32 = tTop - tBuy
		updateRekening(mtopup.Norek, tSaldo)
	} else {
		sError = "1"
	}
	return refid, sError
}

func getHargaTopUp() int {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select hargatopup from tbl_harga"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var htopup int
	for rows.Next() {
		err = rows.Scan(&htopup) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return htopup
}

func insertTranTopup(mtopup MTopup) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into tbl_topup(norek,gram,harga,type)VALUES($1,$2,$3,'topup')"
	_, err := db.Exec(sql, mtopup.Norek, mtopup.Gram, mtopup.Harga)
	if err != nil {
		fmt.Println("sql insert topup : ", err)
	}
}

func insertTran(mtopup MTopup) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into tbl_transaksi(norek,gram,harga,type)VALUES($1,$2,$3,'buyback')"
	_, err := db.Exec(sql, mtopup.Norek, mtopup.Gram, mtopup.Harga)
	if err != nil {
		fmt.Println("sql insert trans : ", err)
	}
}

func getSumTopup(rek string) float32 {
	db := config.ConnectDB()
	defer db.Close()
	sql := "select sum(NULLIF(gram,'')::float) from tbl_topup where norek=$1"
	rows, err := db.Query(sql, rek)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var saldo float32
	for rows.Next() {
		err = rows.Scan(&saldo)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return saldo
}

func getSumBuy(rek string) float32 {
	db := config.ConnectDB()
	defer db.Close()
	sql := "select sum(NULLIF(gram,'')::float) from tbl_transaksi where norek=$1"
	rows, err := db.Query(sql, rek)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var saldo float32
	for rows.Next() {
		err = rows.Scan(&saldo)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return saldo
}

func updateRekening(norek string, saldo float32) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update tbl_rekening set saldo=$1 where norek=$2"
	_, err := db.Exec(sql, saldo, norek)
	if err != nil {
		fmt.Println("sql update rek : ", err)
	}
}

func Saldo(norek string) MSaldo {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select norek,round(saldo::decimal,1) from tbl_rekening where norek=$1"
	//execute sql
	rows, err := db.Query(sql, norek)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var m MSaldo
	for rows.Next() {
		err = rows.Scan(&m.Norek, &m.Saldo) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}

	return m
}
